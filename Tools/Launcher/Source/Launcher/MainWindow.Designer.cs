﻿namespace Launcher
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.WebBrowser = new System.Windows.Forms.WebBrowser();
            this.ProgressTxt = new System.Windows.Forms.Label();
            this.ConsoleBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(11, 106);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(463, 23);
            this.ProgressBar.TabIndex = 0;
            // 
            // WebBrowser
            // 
            this.WebBrowser.Location = new System.Drawing.Point(0, 135);
            this.WebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebBrowser.Name = "WebBrowser";
            this.WebBrowser.ScrollBarsEnabled = false;
            this.WebBrowser.Size = new System.Drawing.Size(485, 85);
            this.WebBrowser.TabIndex = 1;
            // 
            // ProgressTxt
            // 
            this.ProgressTxt.AutoSize = true;
            this.ProgressTxt.Location = new System.Drawing.Point(12, 90);
            this.ProgressTxt.Name = "ProgressTxt";
            this.ProgressTxt.Size = new System.Drawing.Size(74, 13);
            this.ProgressTxt.TabIndex = 2;
            this.ProgressTxt.Text = "Progress: %.2f";
            // 
            // ConsoleBox
            // 
            this.ConsoleBox.BackColor = System.Drawing.SystemColors.Window;
            this.ConsoleBox.Location = new System.Drawing.Point(11, 12);
            this.ConsoleBox.Name = "ConsoleBox";
            this.ConsoleBox.ReadOnly = true;
            this.ConsoleBox.Size = new System.Drawing.Size(463, 75);
            this.ConsoleBox.TabIndex = 3;
            this.ConsoleBox.Text = "";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 222);
            this.Controls.Add(this.ConsoleBox);
            this.Controls.Add(this.ProgressTxt);
            this.Controls.Add(this.WebBrowser);
            this.Controls.Add(this.ProgressBar);
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "COPS v6 - Launcher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.WebBrowser WebBrowser;
        private System.Windows.Forms.Label ProgressTxt;
        private System.Windows.Forms.RichTextBox ConsoleBox;
    }
}