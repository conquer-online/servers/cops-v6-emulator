﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Launcher
{
    public delegate void OnWriteLine(Object Obj);
    public delegate void OnUpdateProgress(Double Percentage);

    public class Program
    {
        public static String AppDirectory;

        [STAThread]
        static void Main(String[] args)
        {
            try
            {
                AppDirectory = Environment.CurrentDirectory;

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainWindow());
            }
            catch (Exception Exc) { MessageBox.Show(Exc.ToString(), "Error!", MessageBoxButtons.OK); }
        }
    }
}
