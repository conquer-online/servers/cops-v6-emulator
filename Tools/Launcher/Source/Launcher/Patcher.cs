﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.ComponentModel;

namespace Launcher
{
    public class Patcher
    {
        private WebClient Client;

        private OnWriteLine WriteLine = null;
        private OnUpdateProgress UpdateProgress = null;

        public Patcher(OnWriteLine WriteLine, OnUpdateProgress UpdateProgress)
        {
            this.WriteLine = WriteLine;
            this.UpdateProgress = UpdateProgress;
        }

        public void CheckVersion(Int32 Version, String URL)
        {
            WriteLine("Checking for a newer version of the client...");
            WriteLine("Download started... Please wait...");

            Client = new WebClient();
            Client.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadCompleted);
            Client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
            Client.DownloadFileAsync(new Uri(URL + Version + ".exe"), Program.AppDirectory + "\\update.exe");
        }

        private void ProgressChanged(Object sender, DownloadProgressChangedEventArgs e)
        {
            UpdateProgress((Double)e.BytesReceived / (Double)e.TotalBytesToReceive * 100.00);
        }

        private void DownloadCompleted(Object sender, AsyncCompletedEventArgs e)
        {
            UpdateProgress(100.00);
            WriteLine("Download Completed!\n");

            FileInfo Info = new FileInfo(Program.AppDirectory + "\\update.exe");
            if (Info.Length > 1000)
                Update();
            else
                Launch();
        }

        private void Update()
        {
            WriteLine("Updating client to a newer version...");
            Process Process = new Process();
            Process.StartInfo.WorkingDirectory = Program.AppDirectory;
            Process.StartInfo.FileName = Program.AppDirectory + "\\update.exe";
            Process.Start();

            Environment.Exit(0);
        }

        private void Launch()
        {
            if (File.Exists(Program.AppDirectory + "\\update.exe"))
                File.Delete(Program.AppDirectory + "\\update.exe");

            WriteLine("Launching game...");
            if (!File.Exists(Program.AppDirectory + "\\Conquer.exe"))
            {
                WriteLine("Executable not found!");
                return;
            }

            Process Process = new Process();
            Process.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
            Process.StartInfo.FileName = Program.AppDirectory + "\\Conquer.exe";
            Process.StartInfo.Arguments = "lightsail";
            Process.Start();

            UInt32 UID = (UInt32)Process.Id;
            Process.Close();

            Injector.WriteLine = WriteLine;
            Injector.StartInjection(Program.AppDirectory + "\\Logik. Defender.dll", UID);
            Thread.Sleep(2500);

            Environment.Exit(0);
        }
    }
}