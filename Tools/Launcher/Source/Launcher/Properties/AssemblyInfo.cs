using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Launcher")]
[assembly: AssemblyDescription("COPS v6 - Launcher")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logik.")]
[assembly: AssemblyProduct("Launcher.exe")]
[assembly: AssemblyCopyright("Copyright © Logik. 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("65116deb-3536-4845-8274-93835fdcf3bb")]

[assembly: AssemblyVersion("1.1.0110.00")]
[assembly: AssemblyFileVersion("1.1.0110.00")]
