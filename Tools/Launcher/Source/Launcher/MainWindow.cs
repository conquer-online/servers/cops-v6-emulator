﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Launcher
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            UpdateProgress(0.00);
            WebBrowser.Navigate("http://copserver.com/launcher/adsense.html");

            Thread Thread = new Thread(Process);
            Thread.IsBackground = true;
            Thread.Start();
        }

        private void Process()
        {
            try
            {
                Thread.Sleep(1500);
                String Text = File.ReadAllText(Program.AppDirectory + "\\version.dat");
                Int32 Version = 0;
                if (!Int32.TryParse(Text, out Version))
                {

                    WriteLine("Invalid version file!");
                    return;
                }
                Version++;

                Patcher Patcher = new Patcher(new OnWriteLine(WriteLine), new OnUpdateProgress(UpdateProgress));
                Patcher.CheckVersion(Version, "http://copserver.com/update/");
            }
            catch (Exception Exc) { MessageBox.Show(Exc.ToString(), "Error!", MessageBoxButtons.OK); }
        }

        private void WriteLine(Object Obj)
        {
            ConsoleBox.AppendText(Obj.ToString() + "\n");
            ConsoleBox.Focus();
            ConsoleBox.SelectionStart = ConsoleBox.Text.Length - 1;
            ConsoleBox.ScrollToCaret();
        }

        private void UpdateProgress(Double Percentage)
        {
            ProgressTxt.Text = String.Format("Progress: {0:F2}%", Percentage);
            ProgressBar.Value = (Int32)Percentage;
        }
    }
}
