﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                String[] Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\ItemAdd.ini", Encoding.Default);
                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\ItemAddition.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Int32)Lines.Length);

                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split(' ');
                        BWriter.Write((Int32)0x4444414B); //KADD
                        BWriter.Write(Int32.Parse(Parts[0] + Parts[1].PadLeft(2, '0'))); //Id + Level[2]
                        BWriter.Write(Int16.Parse(Parts[2])); //Life
                        BWriter.Write(Int16.Parse(Parts[3])); //MaxAtk
                        BWriter.Write(Int16.Parse(Parts[4])); //MinAtk
                        BWriter.Write(Int16.Parse(Parts[5])); //Def
                        BWriter.Write(Int16.Parse(Parts[6])); //MAtk
                        BWriter.Write(Int16.Parse(Parts[7])); //MDef
                        BWriter.Write(Int16.Parse(Parts[8])); //Dexterity
                        BWriter.Write(Int16.Parse(Parts[9])); //Dodge
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
