using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Map")]
[assembly: AssemblyDescription("COPS v6 - Maps Data Creator")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logik.")]
[assembly: AssemblyProduct("Map.exe")]
[assembly: AssemblyCopyright("Copyright © Logik. 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("33e61cf0-c239-45a4-ba30-d29ae764bcb6")]

[assembly: AssemblyVersion("1.0.0312.01")]
[assembly: AssemblyFileVersion("1.0.0312.01")]
