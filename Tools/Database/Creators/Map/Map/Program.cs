﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2010,
// *  Logik, All rights reserved.
// *  
// * ***************************************
// *              SPECIAL THANKS
// * ***************************************
// * Korvacs (Korvacs @ e*pvp)
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2010
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using DMapLoader; // Hacked version of Korvacs' lib

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            String LastMap = "";
            try
            {
                DMapServer Server = new DMapServer();
                Server.ConquerPath = Environment.CurrentDirectory + "\\In\\";
                Server.Output = true;
                Server.Load();

                //String[] Files = Directory.GetFiles(Environment.CurrentDirectory + "\\Out\\");
                //foreach (String Path in Files)
                //{
                //    File.Delete(Path);
                //}

                String[] Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\Map.txt");
                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\Map.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Int32)Lines.Length);

                    foreach (String Line in Lines)
                    {
                        String[] MapInfo = Line.Split(' ');
                        LastMap = MapInfo[0];
                        //using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\" + MapInfo[0] + ".map", FileMode.Create, FileAccess.Write, FileShare.None))
                        //{
                        //    BinaryWriter BWriter = new BinaryWriter(FStream);
                        BWriter.Write((Int32)0x50414D4B); //KMAP
                        BWriter.Write(Int16.Parse(MapInfo[0])); //UniqId
                        BWriter.Write(UInt16.Parse(MapInfo[2])); //Id
                        BWriter.Write(Int32.Parse(MapInfo[3])); //Flags
                        BWriter.Write(Byte.Parse(MapInfo[4])); //Weather
                        BWriter.Write(UInt16.Parse(MapInfo[5])); //PortalX
                        BWriter.Write(UInt16.Parse(MapInfo[6])); //PortalY
                        BWriter.Write(Int16.Parse(MapInfo[7])); //RebornMap
                        BWriter.Write(UInt32.Parse(MapInfo[8])); //Color

                        if (!Server.Maps.ContainsKey(UInt16.Parse(MapInfo[2])))
                        {
                            Console.WriteLine("DMap not found: {0}", UInt16.Parse(MapInfo[2]));
                            BWriter.Write((UInt16)0x00); //Width
                            BWriter.Write((UInt16)0x00); //Height

                            //BWriter.Flush();
                            //BWriter.Close();
                            continue;
                        }

                        DMap DMap = Server.Maps[UInt16.Parse(MapInfo[2])];

                        BWriter.Write((UInt16)DMap.Width); //Width
                        BWriter.Write((UInt16)DMap.Height); //Height

                        for (UInt16 x = 0; x < (UInt16)DMap.Width; x++)
                        {
                            for (UInt16 y = 0; y < (UInt16)DMap.Height; y++)
                                BWriter.Write(DMap.Check(x, y));
                        }

                        for (UInt16 x = 0; x < (UInt16)DMap.Width; x++)
                        {
                            for (UInt16 y = 0; y < (UInt16)DMap.Height; y++)
                                BWriter.Write(DMap.GetHeight(x, y));
                        }
                        //}
                    }
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); Console.WriteLine("Map: " + LastMap); }
            Console.Read();
        }
    }
}
