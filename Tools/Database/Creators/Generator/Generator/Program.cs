﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                String[] Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\Spawns.txt", Encoding.Default);

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\Generator.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Int32)Lines.Length);

                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split('\t');

                        BWriter.Write((Int32)0x4E45474B); //KGEN
                        BWriter.Write(Int16.Parse(Parts[1])); //MapId
                        BWriter.Write(UInt16.Parse(Parts[2])); //StartX
                        BWriter.Write(UInt16.Parse(Parts[3])); //StartY
                        BWriter.Write(UInt16.Parse(Parts[4])); //AddX
                        BWriter.Write(UInt16.Parse(Parts[5])); //AddY
                        BWriter.Write(Int32.Parse(Parts[6])); //MaxNPC
                        BWriter.Write(Int32.Parse(Parts[7])); //Rest Secs
                        BWriter.Write(Int32.Parse(Parts[8])); //Max_Per_Gen
                        BWriter.Write(Int32.Parse(Parts[9])); //NpcType
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
