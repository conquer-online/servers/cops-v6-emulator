﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                IniFile Reader = new IniFile(Environment.CurrentDirectory + "\\In\\AutoAllot.txt");
                Int32 Count = 4;

                String[] Array = new String[] { "10:Trojan", "20:Warrior", "40:Archer", "190:Taoist" };

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\Point_Allot.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Byte)Count); //JobCount
                    BWriter.Write((Byte)120); //LvlCount

                    for (Int32 i = 0; i < Count; i++)
                    {
                        String[] Parts = Array[i].Split(':');
                        Byte JobID = Byte.Parse(Parts[0]);
                        String Section = Parts[1];

                        BWriter.Write((Int32)0x5441504B); //KPAT
                        BWriter.Write((Byte)JobID); //JobID
                        for (Int32 x = 1; x <= 120; x++)
                        {
                            Int16 Strength = Reader.ReadInt16(Section, "Strength[" + x + "]", 0);
                            Int16 Agility = Reader.ReadInt16(Section, "Agility[" + x + "]", 0);
                            Int16 Vitality = Reader.ReadInt16(Section, "Vitality[" + x + "]", 0);
                            Int16 Spirit = Reader.ReadInt16(Section, "Spirit[" + x + "]", 0);

                            BWriter.Write(Strength);
                            BWriter.Write(Agility);
                            BWriter.Write(Vitality);
                            BWriter.Write(Spirit);
                        }
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
