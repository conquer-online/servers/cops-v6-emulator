﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                String[] Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\itemtype.txt", Encoding.Default);
                List<Int32> ItemUID = new List<int>(Lines.Length);

                foreach (String Line in Lines)
                {
                    String[] Parts = Line.Split(' ');
                    ItemUID.Add(Int32.Parse(Parts[0]));
                }

                Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\Lottery.txt", Encoding.Default);

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\Lottery.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Int32)Lines.Length);

                    Int32 i = 0;
                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split(' ');
                        if (!ItemUID.Contains(Int32.Parse(Parts[4])))
                            continue;

                        BWriter.Write((Int32)0x544F4C4B); //KLOT
                        BWriter.Write(i); //Id
                        BWriter.Write(Byte.Parse(Parts[1])); //Rank
                        BWriter.Write(Byte.Parse(Parts[2])); //Chance
                        BWriter.Write(Encoding.Default.GetBytes(Parts[3].PadRight(32, (Char)0x00).Substring(0, 32))); //Prize_Name
                        BWriter.Write(Int32.Parse(Parts[4])); //Prize_Item
                        BWriter.Write(Byte.Parse(Parts[5])); //Hole_Num
                        BWriter.Write(Byte.Parse(Parts[6])); //Addition_Lev
                        i++;
                    }
                    BWriter.Seek(4, SeekOrigin.Begin);
                    BWriter.Write((Int32)i);
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
