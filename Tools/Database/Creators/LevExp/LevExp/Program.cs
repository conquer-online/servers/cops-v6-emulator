﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                String[] Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\LevExp.txt", Encoding.Default);

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\LevExp.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    StreamWriter Writer = new StreamWriter(Environment.CurrentDirectory + "\\Out\\LevExp.sql", false, Encoding.Default);
                    Writer.AutoFlush = true;

                    Writer.WriteLine("INSERT INTO `cq_levexp` (`Id`, `ReqExp`, `ExpTimes`) VALUES");

                    Int32 i = 1;
                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split('\t');
                        String End = ",";
                        if (i % 100 == 0 || i == Lines.Length)
                            End = ";";

                        Writer.Write("(");
                        Writer.Write(Byte.Parse(Parts[0]) + ", "); //Id
                        Writer.Write(UInt64.Parse(Parts[1]) + ", "); //Type
                        Writer.Write(Int32.Parse(Parts[2]) + ")" + End); //Look
                        Writer.WriteLine();

                        if (i % 100 == 0)
                            Writer.WriteLine("INSERT INTO `cq_levexp` (`Id`, `ReqExp`, `ExpTimes`) VALUES");

                        i++;
                    }
                    Writer.Close();

                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Byte)Lines.Length);

                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split('\t');

                        BWriter.Write((Int32)0x4C564C4B); //KLVL
                        BWriter.Write(Byte.Parse(Parts[0])); //Level
                        BWriter.Write(UInt64.Parse(Parts[1])); //Exp
                        BWriter.Write(Int32.Parse(Parts[2])); //Times
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
