﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                String[] Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\Monster.txt", Encoding.Default);
                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\MonsterType.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    StreamWriter Writer = new StreamWriter(Environment.CurrentDirectory + "\\Out\\MonsterType.sql", false, Encoding.Default);
                    Writer.AutoFlush = true;

                    Writer.WriteLine("INSERT INTO `cq_monstertype` (`Id`, `Name`, `Type`, `Look`, `Level`, `Life`, `MinAtk`, `MaxAtk`, `Defense`, `Dodge`, `AtkRange`, `ViewRange`, `AtkSpeed`, `MoveSpeed`, `MagicType`, `MagicDefense`, `DropMoney`, `DropHP`, `DropMP`, `BattleLvl`) VALUES");

                    Int32 i = 1; 
                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split(' ');
                        String End = ",";
                        if (i % 100 == 0 || i == Lines.Length)
                            End = ";";

                        Writer.Write("(");
                        Writer.Write(Int32.Parse(Parts[0]) + ", "); //Id
                        Writer.Write("'" + Parts[1].Substring(0, Math.Min(16, Parts[1].Length)) + "', "); //Name
                        Writer.Write(Byte.Parse(Parts[2]) + ", "); //Type
                        Writer.Write(Int16.Parse(Parts[3]) + ", "); //Look
                        Writer.Write(Int16.Parse(Parts[4]) + ", "); //Level
                        Writer.Write(Int32.Parse(Parts[5]) + ", "); //Life
                        Writer.Write(Int32.Parse(Parts[6]) + ", "); //MinAtk
                        Writer.Write(Int32.Parse(Parts[7]) + ", "); //MaxAtk
                        Writer.Write(Int32.Parse(Parts[8]) + ", "); //Defence
                        Writer.Write(Byte.Parse(Parts[9]) + ", "); //Dodge
                        Writer.Write(Byte.Parse(Parts[10]) + ", "); //AtkRange
                        Writer.Write(Byte.Parse(Parts[11]) + ", "); //ViewRange
                        Writer.Write(Int32.Parse(Parts[12]) + ", "); //AtkSpeed
                        Writer.Write(Int32.Parse(Parts[13]) + ", "); //MoveSpeed
                        Writer.Write(UInt16.Parse(Parts[14]) + ", "); //MagicType
                        Writer.Write(UInt16.Parse(Parts[15]) + ", "); //MagicDefense
                        Writer.Write(Int32.Parse(Parts[16]) + ", "); //DropMoney
                        Writer.Write(Int32.Parse(Parts[17]) + ", "); //DropHP
                        Writer.Write(Int32.Parse(Parts[18]) + ", "); //DropMP
                        Writer.Write(Int16.Parse(Parts[19]) + ")" + End); //BattleLvl
                        Writer.WriteLine();

                        if (i % 100 == 0)
                            Writer.WriteLine("INSERT INTO `cq_monstertype` (`Id`, `Name`, `Type`, `Look`, `Level`, `Life`, `MinAtk`, `MaxAtk`, `Defense`, `Dodge`, `AtkRange`, `ViewRange`, `AtkSpeed`, `MoveSpeed`, `MagicType`, `MagicDefense`, `DropMoney`, `DropHP`, `DropMP`, `BattleLvl`) VALUES");

                        i++;
                    }
                    Writer.Close();

                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Int32)Lines.Length);

                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split(' ');
                        BWriter.Write((Int32)0x424F4D4B); //KMOB
                        BWriter.Write(Int32.Parse(Parts[0])); //Id
                        BWriter.Write(Encoding.Default.GetBytes(Parts[1].PadRight(16, (Char)0x00).Substring(0, 16))); //Name
                        BWriter.Write(Byte.Parse(Parts[2])); //Type
                        BWriter.Write(Int16.Parse(Parts[3])); //Look
                        BWriter.Write(Int16.Parse(Parts[4])); //Level
                        BWriter.Write(Int32.Parse(Parts[5])); //Life
                        BWriter.Write(Int32.Parse(Parts[6])); //MinAtk
                        BWriter.Write(Int32.Parse(Parts[7])); //MaxAtk
                        BWriter.Write(Int32.Parse(Parts[8])); //Defence
                        BWriter.Write(Byte.Parse(Parts[9])); //Dodge
                        BWriter.Write(Byte.Parse(Parts[10])); //AtkRange
                        BWriter.Write(Byte.Parse(Parts[11])); //ViewRange
                        BWriter.Write(Int32.Parse(Parts[12])); //AtkSpeed
                        BWriter.Write(Int32.Parse(Parts[13])); //MoveSpeed
                        BWriter.Write(UInt16.Parse(Parts[14])); //MagicType
                        BWriter.Write(UInt16.Parse(Parts[15])); //MagicDefence
                        BWriter.Write(Int32.Parse(Parts[16])); //DropMoney
                        BWriter.Write(Int32.Parse(Parts[17])); //DropHP
                        BWriter.Write(Int32.Parse(Parts[18])); //DropMP
                        BWriter.Write(Int16.Parse(Parts[19])); //BattleLvl
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
