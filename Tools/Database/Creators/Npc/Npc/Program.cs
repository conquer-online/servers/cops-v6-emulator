﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                String[] Lines = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\NPCs.txt", Encoding.Default);

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\npc.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write((Int32)Lines.Length);

                    foreach (String Line in Lines)
                    {
                        String[] Parts = Line.Split('\t');

                        BWriter.Write((Int32)0x434E504B); //KNPC
                        BWriter.Write(Int32.Parse(Parts[0])); //UniqId
                        BWriter.Write(Encoding.Default.GetBytes(Parts[1].PadRight(16, (Char)0x00).Substring(0, 16))); //Name
                        BWriter.Write(Byte.Parse(Parts[2])); //Type
                        BWriter.Write(Int16.Parse(Parts[3])); //Look
                        BWriter.Write(Int16.Parse(Parts[4])); //MapId
                        BWriter.Write(UInt16.Parse(Parts[5])); //X
                        BWriter.Write(UInt16.Parse(Parts[6])); //Y
                        BWriter.Write(Byte.Parse(Parts[7])); //Base
                        BWriter.Write(Byte.Parse(Parts[8])); //Sort
                        BWriter.Write(Int16.Parse(Parts[9])); //Level
                        BWriter.Write(Int32.Parse(Parts[10])); //Life
                        BWriter.Write(Int16.Parse(Parts[11])); //Defence
                        BWriter.Write(Int16.Parse(Parts[12])); //MagicDef
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
