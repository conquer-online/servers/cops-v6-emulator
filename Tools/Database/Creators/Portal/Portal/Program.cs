﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        struct Coord
        {
            public Int32 X;
            public Int32 Y;
        }

        static void Main(String[] args)
        {
            try
            {
                Dictionary<Int32, Dictionary<Int32, Coord>> DMap = new Dictionary<int, Dictionary<int, Coord>>();

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\In\\ini\\GameMap.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    BinaryReader BReader = new BinaryReader(FStream, Encoding.Default);

                    Int32 Amount = BReader.ReadInt32();
                    for (Int32 i = 0; i < Amount; i++)
                    {
                        Int32 UniqId = BReader.ReadInt32();
                        String Path = Encoding.Default.GetString(BReader.ReadBytes(BReader.ReadInt32()));
                        Dictionary<Int32, Coord> Portals = new Dictionary<int, Coord>();

                        BReader.BaseStream.Seek(4, SeekOrigin.Current);

                        if (DMap.ContainsKey(UniqId))
                            continue;

                        using (FileStream FStream2 = new FileStream(Environment.CurrentDirectory + "\\In\\" + Path, FileMode.Open, FileAccess.Read, FileShare.None))
                        {
                            BinaryReader BReader2 = new BinaryReader(FStream2, Encoding.Default);

                            BReader2.BaseStream.Seek(268, SeekOrigin.Begin);
                            Int32 XCount = BReader2.ReadInt32();
                            Int32 YCount = BReader2.ReadInt32();
                            BReader2.BaseStream.Seek((YCount * 4) + (XCount * YCount * 6), SeekOrigin.Current);

                            Int32 PortalCount = BReader2.ReadInt32();
                            for (Int32 x = 0; x < PortalCount; x++)
                            {
                                Int32 PortalX = BReader2.ReadInt32();
                                Int32 PortalY = BReader2.ReadInt32();
                                Int32 PortalUID = BReader2.ReadInt32();

                                if (!Portals.ContainsKey(PortalUID))
                                    Portals.Add(PortalUID, new Coord() { X = PortalX, Y = PortalY });
                            }
                        }

                        DMap.Add(UniqId, Portals);
                    }
                }

                String[] Passway = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\Passway.txt", Encoding.Default);
                String[] Portal = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\Portal.txt", Encoding.Default);
                String[] Map = File.ReadAllLines(Environment.CurrentDirectory + "\\In\\Map.txt", Encoding.Default);

                List<String> Final = new List<string>();
                foreach (String MapInfo in Map)
                {
                    String[] Parts = MapInfo.Split(' ');
                    Int16 MapUID = Int16.Parse(Parts[0]);
                    Int16 DMapUID = Int16.Parse(Parts[2]);

                    foreach (String PasswayInfo in Passway)
                    {
                        String Info = "";
                        Boolean Ok = false;
                        Info += MapUID.ToString() + " ";

                        String[] Parts2 = PasswayInfo.Split(' ');
                        Int16 PasswayMapId = Int16.Parse(Parts2[1]);

                        if (MapUID != PasswayMapId)
                            continue;

                        if (!DMap.ContainsKey(DMapUID))
                        {
                            Console.WriteLine("DMap: {0}", DMapUID);
                            continue;
                        }

                        Dictionary<Int32, Coord> MapPortals = DMap[DMapUID];
                        if (!MapPortals.ContainsKey(Int32.Parse(Parts2[2])))
                            continue;

                        Coord PortalCoord = MapPortals[Int32.Parse(Parts2[2])];
                        Info += PortalCoord.X.ToString() + " " + PortalCoord.Y.ToString() + " ";
                        Info += Int16.Parse(Parts2[3]).ToString() + " ";

                        foreach (String PortalInfo in Portal)
                        {
                            String[] Parts3 = PortalInfo.Split(' ');
                            Int16 PortalMapId = Int16.Parse(Parts3[1]);
                            Int32 PortalUID = Int32.Parse(Parts3[2]);

                            if (Int16.Parse(Parts3[1]) != Int16.Parse(Parts2[3]))
                                continue;

                            if (Int16.Parse(Parts3[2]) != Int16.Parse(Parts2[4]))
                                continue;

                            Info += Int16.Parse(Parts3[3]).ToString() + " ";
                            Info += Int16.Parse(Parts3[4]).ToString();
                            Ok = true;
                        }

                        if (Ok)
                            if (!Final.Contains(Info))
                                Final.Add(Info);
                    }
                }

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\Portal.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write(Final.Count);

                    foreach (String PortalInfo in Final)
                    {
                        String[] Parts = PortalInfo.Split(' ');

                        BWriter.Write((Int32)0x524F504B); //KPOR
                        BWriter.Write(Int16.Parse(Parts[0])); //MapId
                        BWriter.Write(UInt16.Parse(Parts[1])); //MapX
                        BWriter.Write(UInt16.Parse(Parts[2])); //MapY
                        BWriter.Write(Int16.Parse(Parts[3])); //ToId
                        BWriter.Write(UInt16.Parse(Parts[4])); //ToX
                        BWriter.Write(UInt16.Parse(Parts[5])); //ToY
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
