﻿// * ***************************************
// *              CREDITS
// * ***************************************
// *  Originally created by Jean-Philippe Boivin (CptSky @ e*pvp), Copyright (C) 2011,
// *  Logik, All rights reserved.
// * 
// * ***************************************


// * Created by Jean-Philippe Boivin
// * Copyright © 2011
// * Logik. Project

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Creator
{
    public class Program
    {
        static void Main(String[] args)
        {
            try
            {
                IniFile Reader = new IniFile(Environment.CurrentDirectory + "\\In\\Shop.dat");
                Int32 Count = Reader.ReadInt32("Header", "Amount", 0);

                using (FileStream FStream = new FileStream(Environment.CurrentDirectory + "\\Out\\Goods.pkg", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    BinaryWriter BWriter = new BinaryWriter(FStream, Encoding.Default);
                    BWriter.Write((Int32)0x474B504B); //KPKG
                    BWriter.Write(Count);

                    for (Int32 i = 0; i < Count; i++)
                    {
                        Int32 ID = Reader.ReadInt32("Shop" + i.ToString(), "ID", -1);
                        if (ID == -1)
                            continue;

                        BWriter.Write((Int32)0x4F4F474B); //KGOO
                        BWriter.Write(ID); //ID
                        BWriter.Write(Encoding.Default.GetBytes(Reader.ReadString("Shop" + i.ToString(), "Name", "NULL").PadRight(16, (char)0x00).Substring(0, 16))); //Name
                        BWriter.Write(Reader.ReadByte("Shop" + i.ToString(), "MoneyType", 0)); //Type (0: Silvers, 1: CPs)
                        
                        Byte Amount = Reader.ReadByte("Shop" + i.ToString(), "ItemAmount", 0);
                        BWriter.Write(Amount); //ItemAmount
                        for (Int32 x = 0; x < Amount; x++)
                            BWriter.Write(Reader.ReadInt32("Shop" + i.ToString(), "Item" + x.ToString(), -1));
                    }
                    BWriter.Flush();
                    BWriter.Close();
                }
                Console.WriteLine("Finish!");
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            Console.Read();
        }
    }
}
