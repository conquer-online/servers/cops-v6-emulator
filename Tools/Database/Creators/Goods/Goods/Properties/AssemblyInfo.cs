using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Goods")]
[assembly: AssemblyDescription("COPS v6 - Shop Creator")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logik.")]
[assembly: AssemblyProduct("Goods.exe")]
[assembly: AssemblyCopyright("Copyright © Logik. 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("33e61cf0-c239-45a4-ba30-d29af764bcb6")]

[assembly: AssemblyVersion("1.0.0313.05")]
[assembly: AssemblyFileVersion("1.0.0313.05")]
