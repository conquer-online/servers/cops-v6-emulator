﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IniFile Reader = new IniFile(Application.StartupPath + "\\npc.ini");
                String[] CQ_NPC = File.ReadAllLines(Application.StartupPath + "\\cq_npc.txt", Encoding.Default);
                String[] NPCS = File.ReadAllLines(Application.StartupPath + "\\npcs.txt", Encoding.Default);
                String[] Maps = File.ReadAllLines(Application.StartupPath + "\\map.txt", Encoding.Default);

                List<Int16> AllMaps = new List<short>(Maps.Length);
                foreach (String Map in Maps)
                {
                    String[] Parts = Map.Split(' ');
                    Int16 UniqId = Int16.Parse(Parts[0]);

                    if (!AllMaps.Contains(UniqId))
                        AllMaps.Add(UniqId);
                }

                List<Int32> NPCAdded = new List<int>();
                using (StreamWriter Writer = new StreamWriter(Application.StartupPath + "\\Out.txt", false, Encoding.Default))
                {
                    Writer.AutoFlush = true;
                    foreach (String NPC in CQ_NPC)
                    {
                        String[] Parts = NPC.Split(' ');
                        Int32 UniqId = Int32.Parse(Parts[0]);
                        Int16 MapId = Int16.Parse(Parts[7]);
                        Int16 Look = Int16.Parse(Parts[5]);

                        String Defence = "0";
                        String MagicDef = "0";

                        if (NPCAdded.Contains(UniqId))
                            continue;

                        if (!AllMaps.Contains(MapId))
                            continue;

                        String Name = Reader.ReadString("NpcType" + (Int16)(Look / 10), "Name", "@@ERROR@@");
                        if (Name == "@@ERROR@@")
                            Name = Parts[3];
                        else
                            Name = Name.TrimEnd(' ');
                        Name = Name.PadRight(16, (Char)0x00);

                        if (Parts.Length > 29)
                        {
                            UniqId += 100000;
                            Defence = Parts[29];
                            MagicDef = Parts[30];
                        }

                        //UniqId Name Type Look MapId X Y Base Sort Level Life Defence MagicDef
                        Writer.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}", 
                            UniqId, 
                            Name, 
                            Parts[4].PadLeft(4, '0'), 
                            Look, 
                            MapId, 
                            Parts[8].PadLeft(4, '0'), 
                            Parts[9].PadLeft(4, '0'), 
                            Parts[26].PadLeft(4, '0'), 
                            Parts[27].PadLeft(4, '0'), 
                            Parts[21].PadLeft(4, '0'),
                            Parts[25].PadLeft(4, '0'),
                            Defence.PadLeft(4, '0'),
                            MagicDef.PadLeft(4, '0'));

                        NPCAdded.Add(UniqId);
                    }
                    foreach (String NPC in NPCS)
                    {
                        String[] Parts = NPC.Split(' ');
                        Int32 UniqId = Int32.Parse(Parts[0]);
                        Int16 MapId = Int16.Parse(Parts[7]);
                        Int16 Look = Int16.Parse(Parts[1]);

                        if (NPCAdded.Contains(UniqId))
                            continue;

                        if (!AllMaps.Contains(MapId))
                            continue;

                        String Name = Reader.ReadString("NpcType" + (Int16)(Look / 10), "Name", "@@ERROR@@");
                        if (Name == "@@ERROR@@")
                            Name = Parts[2];
                        else
                            Name = Name.TrimEnd(' ');
                        Name = Name.PadRight(16, (Char)0x00);

                        //UniqId Name Type Look MapId X Y Base Sort
                        Writer.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}", UniqId, Name, Parts[3].PadLeft(4, '0'), Look, MapId, Parts[5].PadLeft(4, '0'), Parts[6].PadLeft(4, '0'), "0000", "0000", "0000", "0000", "0000", "0000");

                        NPCAdded.Add(UniqId);
                    }
                    Writer.Close();
                }
            }
            catch (Exception Exc) { Console.WriteLine(Exc); Console.Read(); }
        }
    }

    class IniFile
    {
        public string FileName;

        public IniFile()
        {
        }

        public IniFile(string _FileName)
        {
            this.FileName = _FileName;
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int GetPrivateProfileStringA(string Section, string Key, string _Default, StringBuilder Buffer, int BufferSize, string FileName);
        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int WritePrivateProfileStringA(string Section, string Key, string Arg, string FileName);

        public byte ReadByte(string Section, string Key, byte _Default)
        {
            byte buf = _Default;
            byte.TryParse(this.ReadString(Section, Key, _Default.ToString(), 6), out buf);
            return buf;
        }

        public short ReadInt16(string Section, string Key, short _Default)
        {
            short buf = _Default;
            short.TryParse(this.ReadString(Section, Key, _Default.ToString(), 9), out buf);
            return buf;
        }

        public int ReadInt32(string Section, string Key, int _Default)
        {
            int buf = _Default;
            int.TryParse(this.ReadString(Section, Key, _Default.ToString(), 15), out buf);
            return buf;
        }

        public sbyte ReadSByte(string Section, string Key, byte _Default)
        {
            sbyte buf = (sbyte)_Default;
            sbyte.TryParse(this.ReadString(Section, Key, _Default.ToString(), 6), out buf);
            return buf;
        }

        public string ReadString(string Section, string Key, string _Default)
        {
            return this.ReadString(Section, Key, _Default, 255);
        }

        public string ReadString(string Section, string Key, string _Default, int BufSize)
        {
            StringBuilder Buffer = new StringBuilder(BufSize);
            GetPrivateProfileStringA(Section, Key, _Default, Buffer, BufSize, this.FileName);
            return Buffer.ToString();
        }

        public ushort ReadUInt16(string Section, string Key, ushort _Default)
        {
            ushort buf = _Default;
            ushort.TryParse(this.ReadString(Section, Key, _Default.ToString(), 9), out buf);
            return buf;
        }

        public uint ReadUInt32(string Section, string Key, uint _Default)
        {
            uint buf = _Default;
            uint.TryParse(this.ReadString(Section, Key, _Default.ToString(), 15), out buf);
            return buf;
        }

        public void Write(string Section, string Key, object Value)
        {
            WritePrivateProfileStringA(Section, Key, Value.ToString(), this.FileName);
        }

        public void Write(string Section, string Key, string Value)
        {
            WritePrivateProfileStringA(Section, Key, Value, this.FileName);
        }

    }
}
