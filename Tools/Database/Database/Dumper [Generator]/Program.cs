﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                String[] CQ_GENERATOR = File.ReadAllLines(Application.StartupPath + "\\cq_generator.txt", Encoding.Default);
                String[] MONSTERS = File.ReadAllLines(Application.StartupPath + "\\monster.txt", Encoding.Default);
                String[] Maps = File.ReadAllLines(Application.StartupPath + "\\map.txt", Encoding.Default);

                List<Int16> AllMaps = new List<short>(Maps.Length);
                foreach (String Map in Maps)
                {
                    String[] Parts = Map.Split(' ');
                    Int16 UniqId = Int16.Parse(Parts[0]);

                    if (!AllMaps.Contains(UniqId))
                        AllMaps.Add(UniqId);
                }

                List<Int32> AllMonsters = new List<Int32>(MONSTERS.Length);
                foreach (String Monster in MONSTERS)
                {
                    String[] Parts = Monster.Split(' ');
                    Int32 UniqId = Int32.Parse(Parts[0]);

                    if (!AllMonsters.Contains(UniqId))
                        AllMonsters.Add(UniqId);
                }

                Int32 UID = 1;
                using (StreamWriter Writer = new StreamWriter(Application.StartupPath + "\\Out.txt", false, Encoding.Default))
                {
                    Writer.AutoFlush = true;
                    foreach (String Spawn in CQ_GENERATOR)
                    {
                        String[] Parts = Spawn.Split(' ');
                        
                        Int16 MapId = Int16.Parse(Parts[1]);
                        UInt16 StartX = UInt16.Parse(Parts[2]);
                        UInt16 StartY = UInt16.Parse(Parts[3]);
                        UInt16 EndX = UInt16.Parse(Parts[4]);
                        UInt16 EndY = UInt16.Parse(Parts[5]);
                        Int32 MaxNPC = Int32.Parse(Parts[6]);
                        Int32 RestSecs = Int32.Parse(Parts[7]);
                        Int32 MaxPerGen = Int32.Parse(Parts[8]);
                        Int32 NpcType = Int32.Parse(Parts[9]);

                        if (!AllMaps.Contains(MapId))
                            continue;

                        if (!AllMonsters.Contains(NpcType))
                            continue;

                        //UniqId MapId StartX StartY EndX EndY MaxNPC MaxPerGen NpcType
                        Writer.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}", 
                            UID,
                            MapId,
                            StartX,
                            StartY,
                            EndX,
                            EndY,
                            MaxNPC,
                            RestSecs,
                            MaxPerGen,
                            NpcType);

                        UID++;
                    }
                    Writer.Close();
                }
            }
            catch (Exception Exc) { Console.WriteLine(Exc); Console.Read(); }
        }
    }
}
