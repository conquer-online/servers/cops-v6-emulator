﻿// * Created by CptSky
// * Copyright © 2011
// * COPS v6 Emulator - Project

using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace COServer
{
    public unsafe partial class Database
    {
        public static Int32[] AllWeaponSkillExp = new Int32[] {
          0,
          1200,
          68000,
          250000,
          640000,
          1600000,
          4000000,
          10000000,
          22000000,
          40000000,
          90000000,
          95000000,
          142500000,
          213750000,
          320625000,
          480937500,
          721406250,
          1082109375,
          1623164063,
          2100000000};
    }
}
