using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("AccServer")]
[assembly: AssemblyDescription("COPS v6 - AccServer (Public)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logik.")]
[assembly: AssemblyProduct("AccServer.exe")]
[assembly: AssemblyCopyright("Copyright © Logik. 2010-2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("7c435ff4-bd97-4b98-b783-2fa24a1cdd37")]

[assembly: AssemblyVersion("1023.2012.0004.0029")]
[assembly: AssemblyFileVersion("1023.2012.0004.0029")]
