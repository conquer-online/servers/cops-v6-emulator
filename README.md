# COPS v6 Emulator

Copyright (C) 2010 CptSky <br />
https://gitlab.com/conquer-online/servers/cops-v6-emulator

## Overview
COPS v6 Emulator is a custom emulator targeting the version 5016 (1056 in French) of Conquer Online 2.0. It was the third attempt by CptSky to write an emulator from scratch, and was the only one used for a live server.

The emulator was developed between December 28th, 2010 and July 4th, 2012 for COPS v6 - The Return Of The Legend. The emulator was developed following the lost of all COPS archives (later partially retrieved), including COPS v5 Emulator, an unreleased emulator targeting the version 1075. Originally only developed for fun, CptSky later decided to launch the final version of the COPS private server using it, after a about a year of shutdown.

## Features

COPS v6 Emulator is an almost full featured emulator. Only a handful of features (like pets) are missing. The emulator implements the official algorithms from the official game, even for uncommon features (for example, the damage on TerrainNPC and experience gains in the Training Ground, the impact of PkPoints with monsters, etc.) This emulator is a really good reference to implement a official-like PvE server.

The emulator implements:
- All the basic features of CO2 (nobility, market, etc.)
- Syndicates
- +10~+12 composition
- 130~140 items [**only with the unreleased custom client**]
- The second reborn
- The prestige reborn (3~10) (Level 130+, for 5 BP)
- Damage and experience based on official algorithms
- New system to help archers (weight for items) [**only with the unreleased custom client**]
- TQ's quests like: MoonBox, Dis City, Ancient Demon, Green Snake
- Some PvP games: City siege (will be remplaced by the GW), Cage Match
- Novice Pack
- New maps and regions [**only with the unreleased custom client**]
- And much more...

## Supported systems

The emulator was developed using Visual Studio 2010 and targets the .NET Framework 4.0. The emulator requires Microsoft Windows (32-bit).